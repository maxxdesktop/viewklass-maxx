############################################################################
#
# Makefile for ViewKlass
#
# Chris Toshok
# Copyright (C) 1995
# The Hungry Programmers, Inc.
# All Rights Reserved

# ViewKlass was created and written by John Lemcke 
#
# Modified by Eric Masson, MaXX Interactive Desktop project
#  (c)  2021
#   website       : https://maxxinteractive.com
#	To contact us : eric.masson@maxxinteractive.com

TAR					= tar -cf
COMPRESS			= gzip -9f
COPY				= cp -f
SED					= sed
COPY_FILE			= $(COPY)
COPY_DIR			= $(COPY) -r
CD_DIR				= cd
INSTALL_FILE		= install -m 644 -p
INSTALL_DIR			= $(COPY_DIR)
INSTALL_PROGRAM		= install -m 755 -p
DEL_FILE			= rm -f
SYMLINK				= ln -sf
DEL_DIR				= rmdir
MOVE				= mv -f
CHK_DIR_EXISTS		= test -d
MKDIR				= mkdir -p
LDCONFIG			= ldconfig

CC				= gcc
CXX				= g++
LINK        	= gcc

RANLIB			= ranlib
AR				= ar



#CXXFLAGS		= -DHAVE_CONFIG_H -m64 -pipe -g -O2 -fno-default-inline -fvisibility=hidden -Wall -W -fpic
#CXXFLAGS		= -DHAVE_CONFIG_H -m${BITS} -pipe -Wall -W -fpic
CXXFLAGS		= -m${BITS} -O3 -Wall -std=c++14 -fPIC -DHAVE_CONFIG_H -g -fstack-protector
CFLAGS			= $(INCFLAGS) -fPIC -Wall -DHAVE_CONFIG_H


##########################################################
#
#  MaXX Interactive Desktop Specifics Env. Variables

BITS			= 64

MAXX_HOME		= /opt/MaXX
MAXX_INCLUDE	= $(MAXX_HOME)/include
MAXX_LIB		= $(MAXX_HOME)/lib$(BITS)
MAXX_BIN		= $(MAXX_HOME)/bin$(BITS)
X11_ROOT		= /usr
#X11_ROOT		= /usr/X11R6           #SLED 10 is still using the old fashion way
X11_INCLUDE		= $(X11_ROOT)/include
X11_LIB			= $(X11_ROOT)/lib$(BITS)
MAXX_MOTIF_HOME 	= $(MAXX_HOME)
MAXX_MOTIF_INCLUDE	= $(MAXX_MOTIF_HOME)/include
MAXX_MOTIF_LIB		= $(MAXX_MOTIF_HOME)/lib$(BITS)


prefix 				= $(MAXX_HOME)

INCLUDES 			= -I. -I$(MAXX_INCLUDE) -I$(MAXX_MOTIF_INCLUDE) -I/usr/include
LIBS 				= -L$(MAXX_MOTIF_LIB) -lXm -L$(MAXX_LIB) -lScheme -lXt -lXp -L$(X11_LIB) -lX11 -lXmu -lstdc++ -lm -lc

exec_prefix 		= ${prefix}

LIBINSTALLDIR		= $(exec_prefix)/lib$(BITS)
INFOINSTALLDIR		= $(prefix)/info
HEADERINSTALLDIR	= $(prefix)/include

################################################################################
#
# You probably should not have to edit anything beyond this point.
#
################################################################################

LIBOBJS= VkCallbackObject.o VkCallbackList.o VkComponent.o VkSimpleWindow.o \
	VkWindow.o VkApp.o VkComponentList.o VkMenuItem.o \
	VkDialogManager.o VkInfoDialog.o VkWarningDialog.o VkErrorDialog.o \
	VkFatalErrorDialog.o VkQuestionDialog.o VkPromptDialog.o \
	VkGenericDialog.o VkFileSelectionDialog.o \
	VkBusyDialog.o VkInterruptDialog.o VkProgressDialog.o \
	VkMessageDialog.o \
	VkSelectionDialog.o VkCursorList.o VkRepeatButton.o \
	VkCheckBox.o VkRadioBox.o VkGangedGroup.o \
	VkRadioGroup.o VkWidgetList.o \
	VkMenu.o VkMenuAction.o VkMenuActionWidget.o \
	VkMenuConfirmFirstAction.o VkMenuActionObject.o \
	VkMenuLabel.o VkMenuToggle.o VkPopupMenu.o \
	VkOptionMenu.o VkMenuBar.o VkMenuSeparator.o VkSubMenu.o  \
	VkRadioSubMenu.o VkMenuUndoManager.o VkHelpPane.o VkResource.o \
	builtinbusy.o VkFormat.o VkPeriodic.o \
	VkPipe.o VkInput.o VkBackground.o VkModel.o \
	VkAction.o VkNameList.o VkSubProcess.o \
	VkPrefDialog.o VkPrefOption.o VkPrefText.o \
	VkPrefToggle.o VkPrefSeparator.o VkPrefCustom.o\
	VkPrefLabel.o VkPrefEmpty.o \
	VkPrefGroup.o VkPrefList.o VkPrefRadio.o VkPrefItem.o \
	VkAlignmentGroup.o VkPixmap.o \
	VkProgram.o \
	VkTrace.o VkCompletionField.o VkHelpAPI.o

LIBHELPOBJS= VkHelpAPI.o


SRCS= $(LIBOBJS:.o=.C)

HDRS= Vk/VkCallbackObject.h Vk/VkCallbackList.h Vk/VkComponent.h \
	Vk/VkSimpleWindow.h \
	Vk/VkWindow.h Vk/VkApp.h Vk/VkComponentList.h Vk/VkMenuItem.h \
	Vk/VkDialogManager.h Vk/VkInfoDialog.h Vk/VkWarningDialog.h \
	Vk/VkErrorDialog.h \
	Vk/VkFatalErrorDialog.h Vk/VkQuestionDialog.h Vk/VkPromptDialog.h \
	Vk/VkGenericDialog.h Vk/VkFileSelectionDialog.h \
	Vk/VkBusyDialog.h Vk/VkInterruptDialog.h Vk/VkProgressDialog.h \
	Vk/VkMessageDialog.h \
	Vk/VkSelectionDialog.h Vk/VkCursorList.h Vk/VkRepeatButton.h \
	Vk/VkCheckBox.h Vk/VkRadioBox.h Vk/VkGangedGroup.h \
	Vk/VkRadioGroup.h Vk/VkWidgetList.h \
	Vk/VkMenu.h Vk/VkMenuAction.h Vk/VkMenuActionWidget.h\
	Vk/VkMenuConfirmFirstAction.h Vk/VkMenuActionObject.h \
	Vk/VkMenuLabel.h Vk/VkMenuToggle.h Vk/VkPopupMenu.h \
	Vk/VkOptionMenu.h Vk/VkMenuBar.h Vk/VkMenuSeparator.h Vk/VkSubMenu.h \
	Vk/VkRadioSubMenu.h Vk/VkMenuUndoManager.h Vk/VkHelpPane.h \
	Vk/VkResource.h \
	Vk/builtinbusy.h Vk/VkFormat.h Vk/VkPeriodic.h \
	Vk/VkPipe.h Vk/VkInput.h Vk/VkBackground.h Vk/VkModel.h \
	Vk/VkAction.h Vk/VkNameList.h Vk/VkSubProcess.h\
	Vk/VkPrefDialog.h Vk/VkPrefOption.h Vk/VkPrefText.h \
	Vk/VkPrefToggle.h Vk/VkPrefSeparator.h Vk/VkPrefCustom.h \
	Vk/VkPrefLabel.h Vk/VkPrefEmpty.h \
	Vk/VkPrefGroup.h Vk/VkPrefList.h Vk/VkPrefRadio.h \
	Vk/VkPrefItem.h \
	Vk/VkAlignmentGroup.h Vk/VkPixmap.h \
	Vk/VkHelpAPI.h \
	Vk/VkProgram.h \
	Vk/VkTrace.h Vk/VkCompletionField.h



LIBSHARED		= libvk.so
LIBHELPSHARED	= libvkhelp.so


TARGET_SO 		= libVk.so.1
TARGET 			= libVk.so.1.0

TARGET_HELP_SO 	= libVkhelp.so.1
TARGET_HELP 	= libVkhelp.so.1.0

INSTALL_LIB 	= $(MAXX_LIB)
INSTALL_INCLUDE = $(MAXX_INCLUDE)/Vk
INSTALL_BIN 	= $(MAXX_BIN)

#------------------------------------------

.C.o:
			$(CXX) $(CXXFLAGS) $(INCLUDES) -c $< -o $@


all:	$(LIBOBJS) $(LIBHELPOBJS)
		$(LINK) -m$(BITS) -shared $(LIBOBJS) $(LIBS) -Wl,-soname -Wl,$(TARGET_SO) -o ./$(TARGET)
		$(LINK) -m$(BITS) -shared $(LIBHELPOBJS) $(LIBS) -Wl,-soname -Wl,$(TARGET_HELP_SO) -o ./$(TARGET_HELP)

vktest: all
		cd test; $(MAKE); cd ..;

	

clean:	
			-$(DEL_FILE) -f $(LIB) $(LIBHELP) $(LIBOBJS) $(LIBHELPOBJS) *.bak *~
			-$(DEL_FILE) -f Vk/*~
			-$(DEL_FILE) -f $(TARGET)
			-$(DEL_FILE) -f $(TARGET_HELP)
			cd test; $(MAKE) clean; cd ..


install: install-lib install-headers

install-lib: 	
				-$(MKDIR) $(INSTALL_LIB)
				-$(INSTALL_PROGRAM) $(TARGET) $(INSTALL_LIB)
				-$(CD_DIR) $(INSTALL_LIB)
				-$(SYMLINK) $(TARGET)  $(INSTALL_LIB)/$(TARGET_SO)
				-$(SYMLINK) $(TARGET_SO)  $(INSTALL_LIB)/$(LIB_NAME).so
				-$(LDCONFIG) 
				-$(CD_DIR) $(CWD)

install-headers: $(HDRS)
				-mkdir -p $(INSTALL_INCLUDE);
				$(INSTALL_FILE) $(HDRS) Vk/VkConfig.h $(INSTALL_INCLUDE)/
