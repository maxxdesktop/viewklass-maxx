# MaXX SGI ViewKit Library

SGI ViewKit compatible library for C++ Xt/Motif development. It was forked from ViewKlass and with the creator's permission.


## Technical Specification
Refer to [MaXX ViewKlass](https://docs.maxxinteractive.com/books/maxx-desktop-frameworks/page/maxx-viewklass) Documentation for more detail.


# Default Settings for building MaXX ViewKlass

### You can edit the Makefile and modify to match your environment.

```
BITS = 64
PREFIX = /opt/MaXX
MOTIF = SGI MOTIF 2.3.8
``` 
 
# To build
```bash
make -j4
su
make install
ldconfig
exit
cd test
make
 
```

