#ifndef NET_MW_SPEC_H
#define NET_MW_SPEC_H

/*  https://specifications.freedesktop.org/wm-spec/1.4/ar01s05.html  */

static const char *UTF8_STRING 		= "UTF8_STRING";

static const char *NET_WM_NAME 		= "_NET_WM_NAME";
static const char *NET_WM_ICON_NAME 	= "_NET_WM_ICON_NAME";
static const char *NET_WM_PID 		= "_NET_WM_PID";

static const char *NET_WM_DESKTOP 	= "_NET_WM_DESKTOP";

/**
 * _NET_WM_WINDOW_TYPE

 _NET_WM_WINDOW_TYPE_DESKTOP, ATOM
_NET_WM_WINDOW_TYPE_DOCK, ATOM
_NET_WM_WINDOW_TYPE_TOOLBAR, ATOM
_NET_WM_WINDOW_TYPE_MENU, ATOM
_NET_WM_WINDOW_TYPE_UTILITY, ATOM
_NET_WM_WINDOW_TYPE_SPLASH, ATOM
_NET_WM_WINDOW_TYPE_DIALOG, ATOM
_NET_WM_WINDOW_TYPE_DROPDOWN_MENU, ATOM
_NET_WM_WINDOW_TYPE_POPUP_MENU, ATOM
_NET_WM_WINDOW_TYPE_TOOLTIP, ATOM
_NET_WM_WINDOW_TYPE_NOTIFICATION, ATOM
_NET_WM_WINDOW_TYPE_COMBO, ATOM
_NET_WM_WINDOW_TYPE_DND, ATOM
_NET_WM_WINDOW_TYPE_NORMAL, ATOM

 _NET_WM_WINDOW_TYPE_DESKTOP indicates a desktop feature. This can include a single window containing desktop icons with the same dimensions as the screen, allowing the desktop environment to have full control of the desktop, without the need for proxying root window clicks.

_NET_WM_WINDOW_TYPE_DOCK indicates a dock or panel feature. Typically a Window Manager would keep such windows on top of all other windows.

_NET_WM_WINDOW_TYPE_TOOLBAR and _NET_WM_WINDOW_TYPE_MENU indicate toolbar and pinnable menu windows, respectively (i.e. toolbars and menus "torn off" from the main application). Windows of this type may set the WM_TRANSIENT_FOR hint indicating the main application window. Note that the _NET_WM_WINDOW_TYPE_MENU should be set on torn-off managed windows, where _NET_WM_WINDOW_TYPE_DROPDOWN_MENU and _NET_WM_WINDOW_TYPE_POPUP_MENU are typically used on override-redirect windows.

_NET_WM_WINDOW_TYPE_UTILITY indicates a small persistent utility window, such as a palette or toolbox. It is distinct from type TOOLBAR because it does not correspond to a toolbar torn off from the main application. It's distinct from type DIALOG because it isn't a transient dialog, the user will probably keep it open while they're working. Windows of this type may set the WM_TRANSIENT_FOR hint indicating the main application window.

_NET_WM_WINDOW_TYPE_SPLASH indicates that the window is a splash screen displayed as an application is starting up.

_NET_WM_WINDOW_TYPE_DIALOG indicates that this is a dialog window. If _NET_WM_WINDOW_TYPE is not set, then managed windows with WM_TRANSIENT_FOR set MUST be taken as this type. Override-redirect windows with WM_TRANSIENT_FOR, but without _NET_WM_WINDOW_TYPE must be taken as _NET_WM_WINDOW_TYPE_NORMAL.

_NET_WM_WINDOW_TYPE_DROPDOWN_MENU indicates that the window in question is a dropdown menu, ie., the kind of menu that typically appears when the user clicks on a menubar, as opposed to a popup menu which typically appears when the user right-clicks on an object. This property is typically used on override-redirect windows.

_NET_WM_WINDOW_TYPE_POPUP_MENU indicates that the window in question is a popup menu, ie., the kind of menu that typically appears when the user right clicks on an object, as opposed to a dropdown menu which typically appears when the user clicks on a menubar. This property is typically used on override-redirect windows.

_NET_WM_WINDOW_TYPE_TOOLTIP indicates that the window in question is a tooltip, ie., a short piece of explanatory text that typically appear after the mouse cursor hovers over an object for a while. This property is typically used on override-redirect windows.

_NET_WM_WINDOW_TYPE_NOTIFICATION indicates a notification. An example of a notification would be a bubble appearing with informative text such as "Your laptop is running out of power" etc. This property is typically used on override-redirect windows.

_NET_WM_WINDOW_TYPE_COMBO should be used on the windows that are popped up by combo boxes. An example is a window that appears below a text field with a list of suggested completions. This property is typically used on override-redirect windows.

_NET_WM_WINDOW_TYPE_DND indicates that the window is being dragged. Clients should set this hint when the window in question contains a representation of an object being dragged from one place to another. An example would be a window containing an icon that is being dragged from one file manager window to another. This property is typically used on override-redirect windows.

_NET_WM_WINDOW_TYPE_NORMAL indicates that this is a normal, top-level window, either managed or override-redirect. Managed windows with neither _NET_WM_WINDOW_TYPE nor WM_TRANSIENT_FOR set MUST be taken as this type. Override-redirect windows without _NET_WM_WINDOW_TYPE, must be taken as this type, whether or not they have WM_TRANSIENT_FOR set. 
 */
static const char *NET_WM_WINDOW_TYPE = "_NET_WM_WINDOW_TYPE";

/**
 * _NET_WM_STATE
 
_NET_WM_STATE_MODAL, ATOM
_NET_WM_STATE_STICKY, ATOM
_NET_WM_STATE_MAXIMIZED_VERT, ATOM
_NET_WM_STATE_MAXIMIZED_HORZ, ATOM
_NET_WM_STATE_SHADED, ATOM
_NET_WM_STATE_SKIP_TASKBAR, ATOM
_NET_WM_STATE_SKIP_PAGER, ATOM
_NET_WM_STATE_HIDDEN, ATOM
_NET_WM_STATE_FULLSCREEN, ATOM
_NET_WM_STATE_ABOVE, ATOM
_NET_WM_STATE_BELOW, ATOM
_NET_WM_STATE_DEMANDS_ATTENTION, ATOM
*/
static const char *NET_WM_STATE = "_NET_WM_STATE";

#endif