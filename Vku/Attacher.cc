#include <Xm/Xm.h>
#include "Attacher.hh"

void
Attacher::attach(Widget w, const char sides[5],
				 Widget top, Widget bot, Widget left, Widget right,
				 int tPos, int bPos, int lPos, int rPos)
{
	if (strlen(sides) < 4) 
	{
		return;
	}

	unsigned char attachments[4];
	
	for (int i = 0; i < 4; ++i) 
	{
		switch (sides[i]) 
		{
		case '-': 
		case 'n': 
		case ' ': 
			attachments[i] = XmATTACH_NONE;
			break;
		case 'f': 
			attachments[i] = XmATTACH_FORM;
			break;
		case 'w': 
			attachments[i] = XmATTACH_WIDGET;
			break;
		case 'p': 
			attachments[i] = XmATTACH_POSITION;
			break;
		case 'F': 
			attachments[i] = XmATTACH_OPPOSITE_FORM;
			break;
		case 'W': 
			attachments[i] = XmATTACH_OPPOSITE_WIDGET;
			break;
		case 's': 
			attachments[i] = XmATTACH_SELF;
			break;
		default:
			attachments[i] = XmATTACH_NONE;
			break;
		}
	}
	XtVaSetValues(w,
				  XmNtopAttachment, attachments[0],
				  XmNtopWidget, top,
				  XmNtopPosition, tPos,
				  XmNbottomAttachment, attachments[1],
				  XmNbottomWidget, bot,
				  XmNbottomPosition, bPos,
				  XmNleftAttachment, attachments[2],
				  XmNleftWidget, left,
				  XmNleftPosition, lPos,
				  XmNrightAttachment,attachments[3],
				  XmNrightWidget, right,
				  XmNrightPosition, rPos,
				  NULL);
}

void
Attacher::offsets(Widget w,
				  int tOffset, int bOffset, int lOffset, int rOffset,
				  Boolean force)
{
	Arg args[4];
	int n = 0;

	if (force) 
	{
		XtVaSetValues(w,
					  XmNtopOffset, tOffset,
					  XmNbottomOffset, bOffset,
					  XmNleftOffset, lOffset,
					  XmNrightOffset, rOffset,
					  NULL);
	}
	else 
	{
		if (tOffset != 0) 
		{
			XtSetArg(args[n], XmNtopOffset, tOffset); ++n;
		}
		if (bOffset != 0) 
		{
			XtSetArg(args[n], XmNbottomOffset, bOffset); ++n;
		}
		if (lOffset != 0) 
		{
			XtSetArg(args[n], XmNleftOffset, lOffset); ++n;
		}
		if (rOffset != 0) 
		{
			XtSetArg(args[n], XmNrightOffset, rOffset); ++n;
		}
		
		XtSetValues(w, args, n);
	}
}
