/**
 *
 * A class to simplify the use attachments in XmForms
 *
 **/

#ifndef ATTACHER_H
#define ATTACHER_H


//#include <Xm/Xm.h>
#include <X11/Intrinsic.h>

class Attacher {
    
public:
	/**
	 * Set the attachments for a Widget.
	 * @param w The widget to attach.
	 * @param sides A string of 4 characters which defines the type
	 * of attachment for each side. sides[0] defines the top attachment,
	 * sides[1] the bottom, sides[2] the left and sides[3] the right.<br />
	 * The valid characters and their meanings are:<pre>
	 * f = ATTACH_FORM
	 * F = ATTACH_OPPOSITE_FORM
	 * w = ATTACH_WIDGET
	 * W = ATTACH_OPPOSITE_WIDGET
	 * p = ATTACH_POSITION
	 * s = ATTACH_SELF
	 * -, n, SPACE = ATTACH_NONE (actually any other character)</pre>
	 * @param top,bot,left.right
	 * The Widgets associated with attachment types w and W.
	 * @param tPos,bPos,lPos,rPos
	 * The positions associated with attachment type p.
	 */
	static void attach(
		Widget w, const char sides[5],
		Widget top = 0, Widget bot = 0, Widget left = 0, Widget right = 0,
		int tPos = 0, int bPos = 0, int lPos = 0, int rPos = 0);

	/**
	 * Sets the attachment offsets for a Widget.
	 * @param w The widget for which to set the offsets.
	 * @param tOffset,bOffset,lOffset,rOffset
	 * The top, bottom, left and right offsets.
	 * @param force When False (the default), this method will only set values
	 * for non-zero arguments. This allows the offsets to be defined in
	 * fallback resources and/or the application's resource file. To get
	 * an offset hard coded to zero set force to True. Note that there is
	 * no way to get one offset set to zero and others to be resource
	 * defined.
	 */
	static void offsets(
		Widget w,
		int tOffset = 0, int bOffset = 0, int lOffset = 0, int rOffset = 0,
		Boolean force = False);
	
	/**
	 * Destructor : does nothing.
	 */
	~Attacher(){}
	
private:
    // constructor
    Attacher();
};

#endif /* ATTACHER_H */
