############################################################################
#
# Makefile for ViewKlass
#
# Chris Toshok
# Copyright (C) 1995
# The Hungry Programmers, Inc.
# All Rights Reserved

# ViewKlass was created and written by John Lemcke 
#
# Modified by Eric Masson, MaXX Interactive Desktop project
#   website       : https://maxxinteractive.com
#	To contact us : eric.masson@maxxinteractive.com
#
#	makefile for the ViewKlass test harness
#

##########################################################
#
#  MaXX Interactive Desktop Specifics Env. Variables

BITS			= 64
MAXX_HOME		= /opt/MaXX
MAXX_INCLUDE	= $(MAXX_HOME)/include
MAXX_LIB		= $(MAXX_HOME)/lib$(BITS)
MAXX_BIN		= $(MAXX_HOME)/bin$(BITS)
X11_ROOT		= /usr
#X11_ROOT		= /usr/X11R6           #SLED 10 is still using the old fashion way
X11_INCLUDE		= $(X11_ROOT)/include
X11_LIB			= $(X11_ROOT)/lib$(BITS)
MAXX_MOTIF_HOME 	= $(MAXX_HOME)
MAXX_MOTIF_INCLUDE	= $(MAXX_MOTIF_HOME)/include
MAXX_MOTIF_LIB		= $(MAXX_MOTIF_HOME)/lib$(BITS)


prefix 				= $(MAXX_HOME)

CCX				= gcc
CXX				= g++
DEFINES			= -fstack-protector -Wwrite-strings
CFLAGS			= -m$(BITS) -O -std=gnu++14 -W $(DEFINES) -fstack-protector
CXXFLAGS		= -m${BITS} -O3 -Wall -std=c++14 -fPIC -DHAVE_CONFIG_H -g -fstack-protector
LFLAGS			= -m$(BITS)
LINK			= g++

MAXX_INCLUDE 	= -I$(MAXX_HOME)/include 
MAXX_LIBS 		= -L$(MAXX_MOTIF_LIB) -lVk -lVkhelp -lXm -L$(MAXX_LIB) -lScheme -lXt -lXp -lXpm -L$(X11_LIB) -lX11 -lXmu -lstdc++ -lm -lc
INCLUDE 		= -I.. $(MAXX_INCLUDE)

LIBDIRS = 		$(MAXX_LIBS)

#LIBS    = -L/opt/MaXX/lib64 -lVk -lVkhelp -lXm -lScheme -lXt -lXp -L/usr/lib64 -lXpm -lXmu -lX11 -lstdc++ -lm -lc

# CCX		= g++  -m64 -pipe -O2 -fno-default-inline -fvisibility=hidden -Wall -W -fPIC
#CCX		= g++  -m${BITS} -Wall -fpic


OBJS	=	vktest.o Slider.o testVkWidgetList.o testVkNameList.o \
			testVkComponentList.o TestPrefDialog.o TestPeriodic.o \
			TestInput.o TestAlignment.o TestCompletion.o TestWindow.o

#LIB_DEPS	= ../libvk.a ../libvkhelp.a

SRCS = $(OBJS:.o=.cpp)

HDRS = 	Slider.h TestPrefDialog.h TestPeriodic.h \
		TestInput.h TestAlignment.h TestCompletion.h TestWindow.h

.cpp.o:
			$(CXX) $(CXXFLAGS) $(INCLUDES) -c $< -o $@
all : vktest staticMenuTest resourceTest


vktest : ${OBJS}
	$(CXX) $(CXXFLAGS) $(OBJS) -Wall -o vktest $(LIBDIRS) $(LIBS)
	

Slider.o : Slider.cpp Slider.h
	${CXX} $(CXXFLAGS) Slider.cpp -c -Wall -o Slider.o $(INCLUDE)

vktest.o : vktest.cpp
	${CXX} $(CXXFLAGS) vktest.cpp -c -Wall -o vktest.o $(INCLUDE)

testVkWidgetList.o : testVkWidgetList.cpp
	${CXX} $(CXXFLAGS) testVkWidgetList.cpp -c -Wall -o testVkWidgetList.o $(INCLUDE)

testVkNameList.o : testVkNameList.cpp
	${CXX} $(CXXFLAGS) testVkNameList.cpp -c -Wall -o testVkNameList.o $(INCLUDE)

testVkComponentList.o : testVkComponentList.cpp
	${CXX} $(CXXFLAGS) testVkComponentList.cpp -c -Wall -o testVkComponentList.o \
	$(INCLUDE)

TestPrefDialog.o : TestPrefDialog.cpp TestPrefDialog.h
	${CCX} $(CXXFLAGS) TestPrefDialog.cpp -c -Wall -o TestPrefDialog.o \
	$(INCLUDE)

TestPeriodic.o : TestPeriodic.cpp TestPeriodic.h
	${CCX} $(CXXFLAGS) TestPeriodic.cpp -c -Wall -o TestPeriodic.o \
	$(INCLUDE)

TestInput.o : TestInput.cpp TestInput.h
	${CCX} $(CXXFLAGS) TestInput.cpp -c -Wall -o TestInput.o \
	$(INCLUDE)

TestAlignment.o : TestAlignment.cpp TestAlignment.h
	${CCX} $(CXXFLAGS) TestAlignment.cpp -c -Wall -o TestAlignment.o \
	$(INCLUDE)

TestCompletion.o : TestCompletion.cpp TestCompletion.h
	${CCX} $(CXXFLAGS) TestCompletion.cpp -c -Wall -o TestCompletion.o \
	$(INCLUDE)

TestWindow.o : TestWindow.cpp TestWindow.h
	${CCX} $(CXXFLAGS) TestWindow.cpp -c -Wall -o TestWindow.o \
	$(INCLUDE)

staticMenuTest : staticMenuTest.o ${LIB_DEPS}
	${CCX} $(CXXFLAGS) staticMenuTest.o -Wall -o staticMenuTest $(LIBDIRS) $(LIBS)

staticMenuTest.o : staticMenuTest.cpp
	${CCX} staticMenuTest.cpp -c -Wall -o staticMenuTest.o $(INCLUDE)

resourceTest : resourceTest.o ${LIB_DEPS}
	${CCX} $(CXXFLAGS) resourceTest.o -Wall -o resourceTest $(LIBDIRS) $(LIBS)

resourceTest.o : resourceTest.cpp
	${CCX} $(CXXFLAGS) resourceTest.cpp -c -Wall -o resourceTest.o $(INCLUDE)

clean :
	-rm -f vktest staticMenuTest resourceTest *.o core *~

